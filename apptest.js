var http = require('http').createServer(handler)
var io = require('socket.io').listen(http);
var fs=require('fs'); 

var people = {};  


http.listen(3000, function(){
	console.log("Conectado");
});

function handler (req, res) {
  fs.readFile(__dirname + '/index.html',
  function (err, data) {
    if (err) {
      res.writeHead(500);
      return res.end('Error loading index.html');
    }

    res.writeHead(200);
    res.end(data);
  });
}


io.on('connection',function(socket){
 
	socket.on('join',function(name){
		people[socket.id]=name;
    io.emit('usuarios',name);
	});


    socket.on('send', function(msg){
       io.emit('chat',people[socket.id],msg);
    });


});




